# Natenju School
La solution de Natenju pour la gestion des établissements scolaires basé sur laravel 5.8.

## Installation
#### 1. Ajouter le paquet
après avoir téléchargé laravel, inclure ce paquet à l'aide de la commande suivante:
```bash
composer require natenju/school
```

#### 2. Configurer l'environnement de base
Après avoir créé la base de données sur le serveur, remplacer les informations ci-dessous dans le fichier `.env`(s'il n'existe pas, dupliquer`.env.example` en le renommant **_.env_**) par les informations de connexion à ladite base.
```
DB_HOST=localhost
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```
pensez également à changer l'url(`APP_URL`) par celle de votre adresse(nom de domaine).
```
APP_URL=http://localhost:8000
```

#### 3. Lancer la commande d'installation
nous pouvons alors installer School avec ou sans données de test.
Pour ajouter des données de test, joindre `--demo` à la fin de la commande.
```bash
php artisan school:install --demo
```
Sinon l'installateur graphique viendra prochainement.
<br><hr>
Aller à l'adresse `domaine.ext` pour le site, ou `domaine.ext/school` si vous n'êtes pas un internaute ordinaire.

Si vous avez installé les données de test, vos coordonées de connexion sont:

>**email:** `admin@school.com`   
>**password:** `password`

Dans le cas ou vous n'avez pas installé de données de test, vou voudriez peut-être faire ceci:
```bash
php artisan school:admin utilisateur@domaine.ext
```
Ceci donnera à un utilisateur les droits d'administration.

Si par contre il n'y a aucun utlisateur dans la base de données, retirez l'adresse mail et remplacez la par `--create`renseignez alors les champs.
