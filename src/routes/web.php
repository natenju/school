<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 12:58 AM
 * @Updated: 3/10/2019 12:58 AM
 * @Desc   : [DESCRIPTION]
 */

$namespacePrefix = '\\' . config('school.controllers.namespace') . '\\';

Route::group(
    ['namespace' => "\Natenju\School\App\Http\Controllers", 'as' => 'school.'],
    function () {
        
        Route::get('login', ['uses' => 'SchoolAuthController@login', 'as' => 'login']);
        Route::post('login', ['uses' => 'SchoolAuthController@postLogin', 'as' => 'postlogin']);
        
        Route::group(
            ['middleware' => 'admin.user'],
            function () {
                Route::get(
                    '/',
                    ['uses' => 'SchoolController@index', 'as' => 'dashboard']
                );
                Route::post('logout', ['uses' => 'SchoolController@logout', 'as' => 'logout']);
                Route::post('upload', ['uses' => 'SchoolController@upload', 'as' => 'upload']);
                Route::get('profile', ['uses' => 'SchoolController@profile', 'as' => 'profile']);
                
                // Role Routes
                Route::resource('roles', 'SchoolRoleController');
                
                // Menu Routes
                Route::group(
                    [
                        'as'     => 'menus.',
                        'prefix' => 'menus/{menu}',
                    ],
                    function () {
                        Route::get(
                            'builder',
                            ['uses' => 'SchoolMenuController@builder', 'as' => 'builder']
                        );
                        Route::post(
                            'order',
                            ['uses' => 'SchoolMenuController@order_item', 'as' => 'order']
                        );
                        
                        Route::group(
                            [
                                'as'     => 'item.',
                                'prefix' => 'item',
                            ],
                            function () {
                                Route::delete(
                                    '{id}',
                                    [
                                        'uses' => 'SchoolMenuController@delete_menu',
                                        'as'   => 'destroy',
                                    ]
                                );
                                Route::post(
                                    '/',
                                    [
                                        'uses' => 'SchoolMenuController@add_item',
                                        'as'   => 'add',
                                    ]
                                );
                                Route::put(
                                    '/',
                                    [
                                        'uses' => 'SchoolMenuController@update_item',
                                        'as'   => 'update',
                                    ]
                                );
                            }
                        );
                    }
                );
                
                // Settings
                Route::group(
                    [
                        'as'     => 'settings.',
                        'prefix' => 'settings',
                    ],
                    function () {
                        Route::get(
                            '/',
                            ['uses' => 'SchoolSettingsController@index', 'as' => 'index']
                        );
                        Route::post(
                            '/',
                            ['uses' => 'SchoolSettingsController@store', 'as' => 'store']
                        );
                        Route::put(
                            '/',
                            ['uses' => 'SchoolSettingsController@update', 'as' => 'update']
                        );
                        Route::delete(
                            '{id}',
                            [
                                'uses' => 'SchoolSettingsController@delete',
                                'as'   => 'delete',
                            ]
                        );
                        Route::get(
                            '{id}/move_up',
                            ['uses' => 'SchoolSettingsController@move_up', 'as' => 'move_up']
                        );
                        Route::get(
                            '{id}/move_down',
                            [
                                'uses' => 'SchoolSettingsController@move_down',
                                'as'   => 'move_down',
                            ]
                        );
                        Route::put(
                            '{id}/delete_value',
                            [
                                'uses' => 'SchoolSettingsController@delete_value',
                                'as'   => 'delete_value',
                            ]
                        );
                    }
                );
                
                // Admin Media
                Route::group(
                    [
                        'as'     => 'media.',
                        'prefix' => 'media',
                    ],
                    function () {
                        Route::get('/', ['uses' => 'VoyagerMediaController@index', 'as' => 'index']);
                        Route::post(
                            'files',
                            ['uses' => 'VoyagerMediaController@files', 'as' => 'files']
                        );
                        Route::post(
                            'new_folder',
                            [
                                'uses' => 'VoyagerMediaController@new_folder',
                                'as'   => 'new_folder',
                            ]
                        );
                        Route::post(
                            'delete_file_folder',
                            [
                                'uses' => 'VoyagerMediaController@delete_file_folder',
                                'as'   => 'delete_file_folder',
                            ]
                        );
                        Route::post(
                            'directories',
                            [
                                'uses' => 'VoyagerMediaController@get_all_dirs',
                                'as'   => 'get_all_dirs',
                            ]
                        );
                        Route::post(
                            'move_file',
                            [
                                'uses' => 'VoyagerMediaController@move_file',
                                'as'   => 'move_file',
                            ]
                        );
                        Route::post(
                            'rename_file',
                            [
                                'uses' => 'VoyagerMediaController@rename_file',
                                'as'   => 'rename_file',
                            ]
                        );
                        Route::post(
                            'upload',
                            ['uses' => 'VoyagerMediaController@upload', 'as' => 'upload']
                        );
                        Route::post(
                            'remove',
                            ['uses' => 'VoyagerMediaController@remove', 'as' => 'remove']
                        );
                        Route::post(
                            'crop',
                            ['uses' => 'VoyagerMediaController@crop', 'as' => 'crop']
                        );
                    }
                );
                
                // Database Routes
                Route::resource('database', 'SchoolDatabaseController');
                
            }
        );
    }
);