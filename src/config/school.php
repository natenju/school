<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 3:51 AM
 * @Updated: 3/10/2019 3:51 AM
 * @Desc   : [DESCRIPTION]
 */

return [
    /*
    |--------------------------------------------------------------------------
    | User config
    |--------------------------------------------------------------------------
    |
    | Here you can specify user configs
    |
    */
    
    'user' => [
        'add_default_role_on_register' => TRUE,
        'default_role'                 => 'user',
        // Set `namespace` to `null` to use `config('auth.providers.users.model')` value
        // Set `namespace` to a class to override auth user model.
        // However make sure the appointed class must ready to use before installing school.
        // Otherwise `php artisan school:install` will fail with class not found error.
        'namespace'                    => NULL,
        'default_avatar'               => 'users/default.png',
        'redirect'                     => '/admin',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Controllers config
    |--------------------------------------------------------------------------
    |
    | Here you can specify controller settings
    |
    */
    
    'controllers' => [
        'namespace' => 'Natenju\\School\\App\\Http\\Controllers',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Models config
    |--------------------------------------------------------------------------
    |
    | Here you can specify default model namespace when creating BREAD.
    | Must include trailing backslashes. If not defined the default application
    | namespace will be used.
    |
    */
    
    'models' => [
        //'namespace' => 'App\\',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Path to the Assets
    |--------------------------------------------------------------------------
    |
    | Here you can specify the location of the assets path
    |
    */
    
    'assets_path' => '/vendor/natenju/school/src/public',
    'themes_path' => '/vendor/natenju/school/src/themes',
    
    /*
    |--------------------------------------------------------------------------
    | Storage Config
    |--------------------------------------------------------------------------
    |
    | Here you can specify attributes related to your application file system
    |
    */
    
    'storage' => [
        'disk' => 'public',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Media Manager
    |--------------------------------------------------------------------------
    |
    | Here you can specify if media manager can show hidden files like(.gitignore)
    |
    */
    
    'hidden_files' => FALSE,
    
    /*
    |--------------------------------------------------------------------------
    | Database Config
    |--------------------------------------------------------------------------
    |
    | Here you can specify database settings
    |
    */
    
    'database' => [
        'tables' => [
            'hidden' => [
                'migrations',
                'data_rows',
                'data_types',
                'menu_items',
                'password_resets',
                'permission_role',
                'settings',
            ],
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Multilingual configuration
    |--------------------------------------------------------------------------
    |
    | Here you can specify if you want to ship with support for
    | multilingual and what locales are enabled.
    |
    */
    
    'multilingual' => [
        /*
         * Set whether or not the multilingual is supported by the BREAD input.
         */
        'enabled' => FALSE,
        
        /*
         * Set whether or not the admin layout default is RTL.
         */
        'rtl'     => FALSE,
        
        /*
         * Select default language
         */
        'default' => 'en',
        
        /*
         * Select languages that are supported.
         */
        'locales' => [
            'en',
            'fr',
            //'pt',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Dashboard config
    |--------------------------------------------------------------------------
    |
    | Here you can modify some aspects of your dashboard
    |
    */
    
    'dashboard' => [
        // Add custom list items to navbar's dropdown
        'navbar_items' => [
            'Profile' => [
                'route'      => 'school.profile',
                'classes'    => 'class-full-of-rum',
                'icon_class' => 'fa-user',
            ],
            'Home'    => [
                'route'        => 'school.dashboard',
                'icon_class'   => 'fa-home',
                'target_blank' => TRUE,
            ],
            'Logout'  => [
                'route'      => 'school.logout',
                'icon_class' => 'fa-power-off',
            ],
        ],
        
        'widgets' => [
        
        ],
    
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Automatic Procedures
    |--------------------------------------------------------------------------
    |
    | When a change happens, we can automate some routines.
    |
    */
    
    'bread' => [
        // When a BREAD is added, create the Menu item using the BREAD properties.
        'add_menu_item'  => TRUE,
        
        // which menu add item to
        'default_menu'   => 'admin',
        
        // When a BREAD is added, create the related Permission.
        'add_permission' => TRUE,
        
        // which role add premissions to
        'default_role'   => 'admin',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | UI Generic Config
    |--------------------------------------------------------------------------
    |
    | Here you change some of the UI settings.
    |
    */
    
    'primary_color' => '#22A7F0',
    
    'show_dev_tips'  => TRUE, // Show development tip "How To Use:" in Menu and Settings
    
    // Here you can specify additional assets you would like to be included in the master.blade
    'additional_css' => [
        //'css/custom.css',
    ],
    
    'additional_js' => [
        //'js/custom.js',
    ],
];