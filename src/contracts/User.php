<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 8:18 PM
 * @Updated: 3/10/2019 8:18 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School\Contracts;


interface User {
    public function role();
    
    public function hasRole($name);
    
    public function setRole($name);
    
    public function hasPermission($name);
    
    public function hasPermissionOrFail($name);
    
    public function hasPermissionOrAbort($name, $statusCode = 403);
}