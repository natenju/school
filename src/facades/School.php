<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 2:26 AM
 * @Updated: 3/10/2019 2:26 AM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School\Facades;


use Illuminate\Support\Facades\Facade;

class School extends Facade {
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
        return 'school';
    }
}