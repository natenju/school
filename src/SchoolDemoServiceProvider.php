<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 2:57 AM
 * @Updated: 3/10/2019 2:57 AM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School;


use Illuminate\Support\ServiceProvider;

/**
 * Class SchoolDemoServiceProvider
 *
 * @package Natenju\School
 */
class SchoolDemoServiceProvider extends ServiceProvider {
    
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        if ( $this->app->runningInConsole() ) {
            $this->registerPublishableResources();
        }
    }
    
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() { }
    
    private function registerPublishableResources() {
        $publishablePath = dirname(__DIR__) . '/../demo';
        
        $publishable = [
            'demo_migrations' => [
                "{$publishablePath}/database/migrations/" => database_path('migrations'),
            ],
            'demo_seeds'      => [
                "{$publishablePath}/database/seeds/" => database_path('seeds'),
            ],
            'demo_content'    => [
                "{$publishablePath}/content/" => storage_path('app/public'),
            ],
        
        ];
        
        foreach ( $publishable as $group => $paths ) {
            $this->publishes($paths, $group);
        }
    }
}
