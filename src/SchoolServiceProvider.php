<?php

namespace Natenju\School;

//use Intervention\Image\ImageServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Natenju\Menu\app\MenuItem;
use Natenju\Menu\MenuServiceProvider;
use Natenju\School\App\Http\Middleware\SchoolAdminMiddleware;
use Natenju\School\Console\Commands\Install;
use Natenju\School\Console\Commands\Uninstall;
use Natenju\School\Console\Commands\Update;
use Natenju\School\facades\School as SchoolFacade;
use Natenju\School\Policies\MenuItemPolicy;
use Natenju\School\policies\SettingPolicy;
use Natenju\Setting\App\Setting;
use Natenju\Setting\SettingServiceProvider;
use Natenju\ThemeInator\ThemeInatorServiceProvider;

/**
 * Class SchoolServiceProvider
 *
 * @package Natenju\School
 */
class SchoolServiceProvider extends ServiceProvider {
    
    /**
     * @var array
     */
    protected $policies = [
        Setting::class  => SettingPolicy::class,
        MenuItem::class => MenuItemPolicy::class,
    ];
    
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        $loader = AliasLoader::getInstance();
        $loader->alias('School', SchoolFacade::class);
        
        $this->app->singleton(
            'school',
            function () {
                return new School();
            }
        );
        //$this->app->register(ImageServiceProvider::class);
        $this->app->register(MenuServiceProvider::class);
        $this->app->register(SettingServiceProvider::class);
        $this->app->register(ThemeInatorServiceProvider::class);
        $this->app->register(SchoolDemoServiceProvider::class);
    }
    
    /**
     * Bootstrap services.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function boot(Router $router) {
        if ( config('school.user.add_default_role_on_register') ) {
            $app_user = config('school.user.namespace') ? : config('auth.providers.users.model');
            $app_user::created(
                function ($user) {
                    if ( is_null($user->role_id) ) {
                        SchoolFacade::model('User')->findOrFail($user->id)
                                    ->setRole(config('school.user.default_role'))
                                    ->save();
                    }
                }
            );
        }
        //if ( config('themes-inator.themes.location') ) {}
        $this->publishes(
            [
                __DIR__ . "/config/school.php" => config_path("school.php"),
            ],
            "school"
        );
        
        $theme = SchoolFacade::getCurrentTheme();
        $this->loadRoutesFrom(__DIR__ . "/routes/web.php");
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadTranslationsFrom(config('themes_path') . $theme . '/resources/lang/', 'school');
        $this->loadViewsFrom(config('themes_path') . $theme . '/resources/views', 'school');
        $router->aliasMiddleware('admin.user', SchoolAdminMiddleware::class);
        if ( $this->app->runningInConsole() ) {
            $this->registerPublishableResources();
            $this->commands(
                [
                    Install::class,
                    Uninstall::class,
                    Update::class,
                ]
            );
        }
        
        $this->loadHelpers();
        $this->registerViewComposers();
    }
    
    /**
     * Load helpers.
     */
    protected function loadHelpers() {
        foreach ( glob(__DIR__ . '/helpers/*.php') as $filename ) {
            if ( !empty($filename) ) {
                require_once $filename;
            }
        }
    }
    
    /**
     * Register view composers.
     */
    protected function registerViewComposers() {
        // Register alerts
        View::composer(
            'school::*',
            function ($view) {
                //$view->with('alerts', SchoolFacade::alerts());
            }
        );
    }
    
    private function registerPublishableResources() {
        $publishablePath = dirname(__DIR__) . '/demo';
        
        $publishable = [
            'school_assets' => [
                "{$publishablePath}/assets/" => public_path(config('school.assets_path')),
            ],
            'school_avatar' => [
                "{$publishablePath}/demo_content/users/" => storage_path('app/public/users'),
            ],
            'seeds'         => [
                "{$publishablePath}/database/seeds/" => database_path('seeds'),
            ],
            'config'        => [
                "{$publishablePath}/config/school.php" => config_path('school.php'),
            ],
        
        ];
        
        foreach ( $publishable as $group => $paths ) {
            $this->publishes($paths, $group);
        }
    }
}
