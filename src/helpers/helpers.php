<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 10:03 PM
 * @Updated: 3/10/2019 10:03 PM
 * @Desc   : [DESCRIPTION]
 */

use Natenju\School\Facades\School as SchoolFacade;

if ( !function_exists('setting') ) {
    function setting($key, $default = NULL) {
        return SchoolFacade::setting($key, $default);
    }
}

if ( !function_exists('menu') ) {
    function menu($menuName, $type = NULL, array $options = []) {
        return SchoolFacade::model('Menu')->display($menuName, $type, $options);
    }
}

if ( !function_exists('school_asset') ) {
    function school_asset($path, $secure = NULL) {
        //return asset(config('school.assets_path') . '/' . $path, $secure);
        return school_theme(setting('theme', "flat_admin_3")) . "/public" . setting('theme', "flat_admin_3") . $path;
    }
}

if ( !function_exists('school_theme') ) {
    function school_theme($theme, $secure = NULL) {
        return asset(config('school.themes_path') . '/' . $theme, $secure);
    }
}
