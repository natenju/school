<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 8:28 PM
 * @Updated: 3/10/2019 8:28 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School\policies;


use Natenju\School\Contracts\User;

/**
 * Class SettingPolicy
 *
 * @package Natenju\School\policies
 */
class SettingPolicy extends BasePolicy {
    /**
     * @param \Natenju\School\Contracts\User  $user
     * @param                                 $model
     *
     * @return mixed
     */
    public function browse(User $user, $model) {
        return $user->hasPermission("browse_settings");
    }
    
    /**
     * @param \Natenju\School\Contracts\User  $user
     * @param                                 $model
     *
     * @return mixed
     */
    public function read(User $user, $model) {
        return $user->hasPermission('read_settings');
    }
    
    /**
     * @param \Natenju\School\Contracts\User  $user
     * @param                                 $model
     *
     * @return mixed
     */
    public function edit(User $user, $model) {
        return $user->hasPermission('edit_settings');
    }
    
    /**
     * @param \Natenju\School\Contracts\User  $user
     * @param                                 $model
     *
     * @return mixed
     */
    public function add(User $user, $model) {
        return $user->hasPermission('add_settings');
    }
    
    /**
     * @param \Natenju\School\Contracts\User  $user
     * @param                                 $model
     *
     * @return mixed
     */
    public function delete(User $user, $model) {
        return $user->hasPermission('delete_settings');
    }
}