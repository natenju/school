<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 8:39 PM
 * @Updated: 3/10/2019 8:39 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School\Policies;


use Natenju\School\Contracts\User as UserContract;
use Natenju\School\Facades\School as SchoolFacade;

class MenuItemPolicy extends BasePolicy {
    protected static $permissions = NULL;
    
    protected function checkPermission(UserContract $user, $model, $action) {
        if ( self::$permissions == NULL ) {
            self::$permissions = SchoolFacade::model('Permission')->all();
        }
        
        $regex = str_replace('/', '\/', preg_quote(route('school.dashboard')));
        $slug = preg_replace('/' . $regex . '/', '', $model->link(TRUE));
        $slug = str_replace('/', '', $slug);
        
        if ( $slug == '' ) {
            $slug = 'admin';
        }
        
        if ( empty($action) ) {
            $action = 'browse';
        }
        
        // If permission doesn't exist, we can't check it!
        if ( !self::$permissions->contains('key', $action . '_' . $slug) ) {
            return TRUE;
        }
        
        return $user->hasPermission($action . '_' . $slug);
    }
}