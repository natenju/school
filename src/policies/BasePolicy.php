<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 8:14 PM
 * @Updated: 3/10/2019 8:14 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School\Policies;


use Illuminate\Auth\Access\HandlesAuthorization;

class BasePolicy {
    use HandlesAuthorization;
    
    public function __call($name, $arguments) {
        if ( count($arguments) < 2 ) {
            throw new \InvalidArgumentException('not enough arguments');
        }
        /**
         * @var \Natenju\School\Contracts\User $user
         */
        $user = $arguments[0];
        
        /** @var $model */
        $model = $arguments[1];
        
        return $user->hasPermission($name . '_' . $model);
    }
}