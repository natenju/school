<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 2:24 AM
 * @Updated: 3/10/2019 2:24 AM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School;


use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Natenju\Menu\App\MenuItem;
use Natenju\Menu\App\Menu;
use Natenju\School\app\Permission;
use Natenju\School\app\Role;
use Natenju\School\app\User;
use Natenju\Setting\App\Setting;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class School {
    
    protected $version;
    protected $filesystem;
    
    protected $permissionsLoaded = FALSE;
    protected $permissions       = [];
    protected $users             = [];
    protected $models            = [
        //'Category'    => Category::class,
        //'DataRow'     => DataRow::class,
        //'DataType'    => DataType::class,
        'Menu'        => Menu::class,
        'MenuItem'    => MenuItem::class,
        //'Page'        => Page::class,
        'Permission'  => Permission::class,
        //'Post'        => Post::class,
        'Role'        => Role::class,
        'Setting'     => Setting::class,
        'User'        => User::class,
        //'Translation' => Translation::class,
    ];
    
    public $setting_cache = NULL;
    
    /**
     * School constructor.
     */
    public function __construct() {
        $this->filesystem = app(Filesystem::class);
        $this->findVersion();
    }
    
    public static function routes() {
        require __DIR__ . '/routes/web.php';
    }
    
    public function getVersion() {
        return $this->version;
    }
    
    public function getLocales() {
        //TODO: make locales path match current theme folder
        return array_diff(scandir(realpath(__DIR__ ."/themes/".$this->getCurrentTheme(). '/resources/lang')), ['..', '.']);
    }
    
    /**
     * @return null|string Current user theme or default
     */
    public function getCurrentTheme() {
        return $this->setting('theme', "flat_admin_3");
    }
    
    /**
     * Scan /themes folder to gather'em all
     * @return array all themes
     */
    public function getThemes() {
        return array_diff(scandir(realpath(__DIR__ . '/themes')), ['..', '.']);
    }
    
    /**
     * @param null $id
     *
     * @return mixed
     */
    protected function getUser($id = NULL) {
        if ( is_null($id) ) {
            $id = auth()->check() ? auth()->user()->id : NULL;
        }
        
        if ( is_null($id) ) {
            return NULL;
        }
        
        if ( !isset($this->users[$id]) ) {
            $this->users[$id] = self::model('User')->find($id);
        }
        
        return $this->users[$id];
    }
    
    protected function findVersion() {
        if ( !is_null($this->version) ) {
            return;
        }
        
        if ( $this->filesystem->exists(base_path('composer.lock')) ) {
            // Get the composer.lock file
            $file = json_decode(
                $this->filesystem->get(base_path('composer.lock'))
            );
            
            // Loop through all the packages and get the version of our package
            foreach ( $file->packages as $package ) {
                if ( $package->name == 'natenju/school' ) {
                    $this->version = $package->version;
                    break;
                }
            }
        }
    }
    
    public function image($file, $default = '') {
        if ( !empty($file) ) {
            return str_replace('\\', '/', Storage::disk(config('school.storage.disk'))->url($file));
        }
        
        return $default;
    }
    
    public function model($name) {
        return app($this->models[studly_case($name)]);
    }
    
    /**
     * returns the $model class corresponding to  the key $name
     *
     * @example $models=['Category'=>Category::class] returns Category::Class
     *
     * @param $name
     *
     * @return mixed
     */
    public function modelClass($name) {
        return $this->models[$name];
    }
    
    /**
     * @param $name
     * @param $object
     *
     * @return $this
     * @throws \Exception
     */
    public function useModel($name, $object) {
        if ( is_string($object) ) {
            $object = app($object);
        }
        
        $class = get_class($object);
        
        if ( isset($this->models[studly_case($name)]) && !$object instanceof $this->models[studly_case($name)] ) {
            throw new \Exception("[{$class}] must be instance of [{$this->models[studly_case($name)]}].");
        }
        
        $this->models[studly_case($name)] = $class;
        
        return $this;
    }
    
    /**
     * @param      $key
     * @param null $default
     *
     * @return |null
     */
    public function setting($key, $default = NULL) {
        if ( $this->setting_cache === NULL ) {
            foreach ( self::model('Setting')->all() as $setting ) {
                $keys = explode('.', $setting->key);
                @$this->setting_cache[$keys[0]][$keys[1]] = $setting->value;
            }
        }
        
        $parts = explode('.', $key);
        
        if ( count($parts) == 2 ) {
            return @$this->setting_cache[$parts[0]][$parts[1]] ? : $default;
        } else {
            return @$this->setting_cache[$parts[0]] ? : $default;
        }
    }
    
    /** @deprecated */
    protected function loadPermissions() {
        if ( !$this->permissionsLoaded ) {
            $this->permissionsLoaded = TRUE;
            
            $this->permissions = self::model('Permission')->all();
        }
    }
    
    /** @deprecated
     * @param $permission
     *
     * @return bool
     * @throws \Exception
     */
    public function can($permission) {
        $this->loadPermissions();
        
        // Check if permission exist
        $exist = $this->permissions->where('key', $permission)->first();
        
        // Permission not found
        if ( !$exist ) {
            throw new \Exception('Permission does not exist', 400);
        }
        
        $user = $this->getUser();
        if ( $user == NULL || !$user->hasPermission($permission) ) {
            return FALSE;
        }
        
        return TRUE;
    }
    
    /** @deprecated
     * @param $permission
     *
     * @return bool
     * @throws \Exception
     */
    public function canOrFail($permission) {
        if ( !$this->can($permission) ) {
            throw new AccessDeniedHttpException();
        }
        
        return TRUE;
    }
    
    /** @deprecated
     * @param     $permission
     * @param int $statusCode
     *
     * @return bool
     * @throws \Exception
     */
    public function canOrAbort($permission, $statusCode = 403) {
        if ( !$this->can($permission) ) {
            abort($statusCode);
        }
        
        return TRUE;
    }
    
}