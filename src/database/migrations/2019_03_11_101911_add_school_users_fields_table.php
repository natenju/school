<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSchoolUsersFieldsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table(
            'users',
            function (Blueprint $table) {
                if ( !Schema::hasColumn('users', 'avatar') ) {
                    $table->string('avatar')->nullable()->after('email')->default('users/default.png');
                }
                if ( !Schema::hasColumn('users', 'role_id') ) {
                    $table->unsignedBigInteger('role_id')->nullable()->after('id');
                }
                if ( !Schema::hasColumn('users', 'email_verified_at') ) {
                    $table->timestamp('email_verified_at')->nullable();
                }
                $table->text('settings')->nullable();
                $table->string('id_card')->nullable();
                $table->date('date_of_birth')->nullable();
                $table->string('phone_number')->nullable();
                $table->string('address')->nullable();
                $table->date('joining_date')->nullable();
                $table->string('signature')->nullable();
                $table->string('twitter')->nullable();
                $table->string('google')->nullable();
                $table->string('facebook')->nullable();
                
                $table->unsignedSmallInteger('gender_id')->nullable();
                $table->unsignedSmallInteger('religion_id')->nullable();
                
                $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
                $table->foreign('gender_id')->references('id')->on('genders')->onDelete('cascade');
                $table->foreign('religion_id')->references('id')->on('religions')->onDelete('cascade');
            }
        );
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if ( Schema::hasColumn('users', 'avatar') ) {
            Schema::table(
                'users',
                function ($table) {
                    $table->dropColumn('avatar');
                }
            );
        }
        if ( Schema::hasColumn('users', 'role_id') ) {
            Schema::table(
                'users',
                function ($table) {
                    $table->dropColumn('role_id');
                }
            );
        }
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->dropForeign(['role_id']);
                $table->integer('role_id')->change();
            }
        );
    }
}
