<?php

use Illuminate\Database\Seeder;
use Natenju\Menu\App\Menu;

class MenusTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Menu::firstOrCreate(
            [
                'name' => 'admin',
            ]
        );
    }
}
