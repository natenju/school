<?php

use Illuminate\Database\Seeder;
use Natenju\Menu\App\Menu;
use Natenju\Menu\App\MenuItem;

class MenuItemsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $menu = Menu::where('name', 'admin')->firstOrFail();
    
        $menuItem = MenuItem::firstOrNew(
            [
                'menu_id' => $menu->id,
                'title'   => __('school::seeders.menu_items.dashboard'),
                'url'     => '',
                'route'   => 'school.dashboard',
            ]
        );
        if ( !$menuItem->exists ) {
            $menuItem->fill(
                [
                    'target'     => '_self',
                    'icon_class' => 'fa-tachometer',
                    'color'      => NULL,
                    'parent_id'  => NULL,
                    'order'      => 1,
                ]
            )->save();
        }
    
        $menuItem = MenuItem::firstOrNew(
            [
                'menu_id' => $menu->id,
                'title'   => __('school::seeders.menu_items.media'),
                'url'     => '',
                'route'   => 'school.media.index',
            ]
        );
        if ( !$menuItem->exists ) {
            $menuItem->fill(
                [
                    'target'     => '_self',
                    'icon_class' => 'fa-images',
                    'color'      => NULL,
                    'parent_id'  => NULL,
                    'order'      => 5,
                ]
            )->save();
        }
    
        $menuItem = MenuItem::firstOrNew(
            [
                'menu_id' => $menu->id,
                'title'   => __('school::seeders.menu_items.users'),
                'url'     => '',
                'route'   => 'school.users.index',
            ]
        );
        if ( !$menuItem->exists ) {
            $menuItem->fill(
                [
                    'target'     => '_self',
                    'icon_class' => 'fa-person',
                    'color'      => NULL,
                    'parent_id'  => NULL,
                    'order'      => 3,
                ]
            )->save();
        }
    
        $menuItem = MenuItem::firstOrNew(
            [
                'menu_id' => $menu->id,
                'title'   => __('school::seeders.menu_items.roles'),
                'url'     => '',
                'route'   => 'school.roles.index',
            ]
        );
        if ( !$menuItem->exists ) {
            $menuItem->fill(
                [
                    'target'     => '_self',
                    'icon_class' => 'fa-lock',
                    'color'      => NULL,
                    'parent_id'  => NULL,
                    'order'      => 2,
                ]
            )->save();
        }
    
        $toolsMenuItem = MenuItem::firstOrNew(
            [
                'menu_id' => $menu->id,
                'title'   => __('school::seeders.menu_items.tools'),
                'url'     => '',
            ]
        );
        if ( !$toolsMenuItem->exists ) {
            $toolsMenuItem->fill(
                [
                    'target'     => '_self',
                    'icon_class' => 'fa-tools',
                    'color'      => NULL,
                    'parent_id'  => NULL,
                    'order'      => 9,
                ]
            )->save();
        }
    
        $menuItem = MenuItem::firstOrNew(
            [
                'menu_id' => $menu->id,
                'title'   => __('school::seeders.menu_items.menu_builder'),
                'url'     => '',
                'route'   => 'school.menus.index',
            ]
        );
        if ( !$menuItem->exists ) {
            $menuItem->fill(
                [
                    'target'     => '_self',
                    'icon_class' => 'fa-list',
                    'color'      => NULL,
                    'parent_id'  => $toolsMenuItem->id,
                    'order'      => 10,
                ]
            )->save();
        }
    
        $menuItem = MenuItem::firstOrNew(
            [
                'menu_id' => $menu->id,
                'title'   => __('school::seeders.menu_items.database'),
                'url'     => '',
                'route'   => 'school.database.index',
            ]
        );
        if ( !$menuItem->exists ) {
            $menuItem->fill(
                [
                    'target'     => '_self',
                    'icon_class' => 'fa-data',
                    'color'      => NULL,
                    'parent_id'  => $toolsMenuItem->id,
                    'order'      => 11,
                ]
            )->save();
        }
    
        $menuItem = MenuItem::firstOrNew(
            [
                'menu_id' => $menu->id,
                'title'   => __('school::seeders.menu_items.settings'),
                'url'     => '',
                'route'   => 'school.settings.index',
            ]
        );
        if ( !$menuItem->exists ) {
            $menuItem->fill(
                [
                    'target'     => '_self',
                    'icon_class' => 'fa-cog',
                    'color'      => NULL,
                    'parent_id'  => NULL,
                    'order'      => 14,
                ]
            )->save();
        }
    }
}
