<?php

use Illuminate\Database\Seeder;
use Natenju\School\app\Permission;
use Natenju\School\app\Role;

class PermissionRoleTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $role = Role::where('name', 'admin')->firstOrFail();
    
        $permissions = Permission::all();
    
        $role->permissions()->sync(
            $permissions->pluck('id')->all()
        );
    }
}
