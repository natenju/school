<?php

use Illuminate\Database\Seeder;
use Natenju\Setting\App\Setting;

class SettingsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    /**
     * Auto generated seed file.
     */
    public function run() {
        $setting = $this->findSetting('site.title');
        if ( !$setting->exists ) {
            $setting->fill(
                [
                    'display_name' => __('school::seeders.settings.site.title'),
                    'value'        => __('school::seeders.settings.site.title'),
                    'details'      => '',
                    'type'         => 'text',
                    'order'        => 1,
                    'group'        => 'Site',
                ]
            )->save();
        }
        
        $setting = $this->findSetting('site.description');
        if ( !$setting->exists ) {
            $setting->fill(
                [
                    'display_name' => __('school::seeders.settings.site.description'),
                    'value'        => __('school::seeders.settings.site.description'),
                    'details'      => '',
                    'type'         => 'text',
                    'order'        => 2,
                    'group'        => 'Site',
                ]
            )->save();
        }
        
        $setting = $this->findSetting('site.logo');
        if ( !$setting->exists ) {
            $setting->fill(
                [
                    'display_name' => __('school::seeders.settings.site.logo'),
                    'value'        => '',
                    'details'      => '',
                    'type'         => 'image',
                    'order'        => 3,
                    'group'        => 'Site',
                ]
            )->save();
        }
        
        $setting = $this->findSetting('site.google_analytics_tracking_id');
        if ( !$setting->exists ) {
            $setting->fill(
                [
                    'display_name' => __('school::seeders.settings.site.google_analytics_tracking_id'),
                    'value'        => '',
                    'details'      => '',
                    'type'         => 'text',
                    'order'        => 4,
                    'group'        => 'Site',
                ]
            )->save();
        }
        
        $setting = $this->findSetting('admin.bg_image');
        if ( !$setting->exists ) {
            $setting->fill(
                [
                    'display_name' => __('school::seeders.settings.admin.background_image'),
                    'value'        => '',
                    'details'      => '',
                    'type'         => 'image',
                    'order'        => 5,
                    'group'        => 'Admin',
                ]
            )->save();
        }
        
        $setting = $this->findSetting('admin.title');
        if ( !$setting->exists ) {
            $setting->fill(
                [
                    'display_name' => __('school::seeders.settings.admin.title'),
                    'value'        => 'School',
                    'details'      => '',
                    'type'         => 'text',
                    'order'        => 1,
                    'group'        => 'Admin',
                ]
            )->save();
        }
        
        $setting = $this->findSetting('admin.description');
        if ( !$setting->exists ) {
            $setting->fill(
                [
                    'display_name' => __('school::seeders.settings.admin.description'),
                    'value'        => __('school::seeders.settings.admin.description_value'),
                    'details'      => '',
                    'type'         => 'text',
                    'order'        => 2,
                    'group'        => 'Admin',
                ]
            )->save();
        }
        
        $setting = $this->findSetting('admin.loader');
        if ( !$setting->exists ) {
            $setting->fill(
                [
                    'display_name' => __('school::seeders.settings.admin.loader'),
                    'value'        => '',
                    'details'      => '',
                    'type'         => 'image',
                    'order'        => 3,
                    'group'        => 'Admin',
                ]
            )->save();
        }
        
        $setting = $this->findSetting('admin.icon_image');
        if ( !$setting->exists ) {
            $setting->fill(
                [
                    'display_name' => __('school::seeders.settings.admin.icon_image'),
                    'value'        => '',
                    'details'      => '',
                    'type'         => 'image',
                    'order'        => 4,
                    'group'        => 'Admin',
                ]
            )->save();
        }
        
        $setting = $this->findSetting('admin.google_analytics_client_id');
        if ( !$setting->exists ) {
            $setting->fill(
                [
                    'display_name' => __('school::seeders.settings.admin.google_analytics_client_id'),
                    'value'        => '',
                    'details'      => '',
                    'type'         => 'text',
                    'order'        => 1,
                    'group'        => 'Admin',
                ]
            )->save();
        }
        
        $setting = $this->findSetting('admin.theme');
        if ( !$setting->exists ) {
            $setting->fill(
                [
                    'display_name' => __('school::seeders.settings.admin.theme'),
                    'value'        => '',
                    'details'      => '',
                    'type'         => 'text',
                    'order'        => 1,
                    'group'        => 'Admin',
                ]
            )->save();
        }
    }
    
    /**
     * first or create new setting item.
     *
     * @param   String $key the key to setting
     *
     * @return  \Illuminate\Database\Eloquent\Model the model corresponding to the key given
     */
    protected function findSetting($key) {
        return Setting::firstOrNew(['key' => $key]);
    }
}
