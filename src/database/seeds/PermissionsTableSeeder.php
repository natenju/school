<?php

use Illuminate\Database\Seeder;
use Natenju\School\app\Permission;

class PermissionsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
    
        $keys = [
            'browse_admin',
            //'browse_bread',
            'browse_database',
            'browse_media',
            //'browse_compass',
        ];
    
        foreach ( $keys as $key ) {
            Permission::firstOrCreate(
                [
                    'key'        => $key,
                    'table_name' => NULL,
                ]
            );
        }
    
        Permission::generateFor('menus');
    
        Permission::generateFor('roles');
    
        Permission::generateFor('users');
    
        Permission::generateFor('settings');
    }
}
