<?php

use Illuminate\Database\Seeder;
use Natenju\School\app\Role;
use Natenju\School\app\User;

class UsersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if ( User::count() == 0 ) {
            $role = Role::where('name', 'admin')->firstOrFail();
        
            User::create(
                [
                    'name'           => 'Admin',
                    'email'          => 'admin@school.com',
                    'password'       => bcrypt('password'),
                    'remember_token' => str_random(60),
                    'role_id'        => $role->id,
                ]
            );
        }
    }
}
