<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/11/2019 8:03 AM
 * @Updated: 3/11/2019 8:03 AM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School\App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;

/**
 * Class Controller
 *
 * @package Natenju\School\app\Http\Controllers
 */
abstract class Controller extends BaseController {
    use AuthorizesRequests,
        ValidatesRequests,
        DispatchesJobs;
    
    public function getSlug(Request $request) {
        if ( isset($this->slug) ) {
            $slug = $this->slug;
        } else {
            $slug = explode('.', $request->route()->getName())[1];
        }
        
        return $slug;
    }
    
    public function insertUpdateData($request, $slug, $rows, $data) {
        $multi_select = [];
        
        $data->save();
        return $data;
    }
    
    public function deleteFileIfExists($path) {
        if ( Storage::disk(config('school.storage.disk'))->exists($path) ) {
            Storage::disk(config('school.storage.disk'))->delete($path);
        }
    }
    
    /**
     * Get fields having validation rules in proper format.
     *
     * @param array $fieldsConfig
     *
     * @return \Illuminate\Support\Collection
     */
    protected function getFieldsWithValidationRules($fieldsConfig) {
        return $fieldsConfig->filter(
            function ($value) {
                if ( empty($value->details) ) {
                    return FALSE;
                }
                
                return !empty($value->details->validation->rule);
            }
        );
    }
}