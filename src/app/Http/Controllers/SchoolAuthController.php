<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/11/2019 9:24 AM
 * @Updated: 3/11/2019 9:24 AM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School\App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class SchoolAuthController
 *
 * @package Natenju\School\app\Http\Controllers
 */
class SchoolAuthController extends Controller {
    use AuthenticatesUsers;
    
    public function login() {
        if ( Auth::user() ) {
            return redirect()->route('school.dashboard');
        }
        
        return view('school::themes.flat_admin_3.resources.views.auth.login');
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response|void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postLogin(Request $request) {
        $this->validateLogin($request);
        
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ( $this->hasTooManyLoginAttempts($request) ) {
            $this->fireLockoutEvent($request);
            
            return $this->sendLockoutResponse($request);
        }
        
        $credentials = $this->credentials($request);
        
        if ( $this->guard()->attempt($credentials, $request->has('remember')) ) {
            return $this->sendLoginResponse($request);
        }
        
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);
        
        return $this->sendFailedLoginResponse($request);
    }
    
    /*
     * Preempts $redirectTo member variable (from RedirectsUsers trait)
     */
    public function redirectTo() {
        return config('school.user.redirect', route('school.dashboard'));
    }
}