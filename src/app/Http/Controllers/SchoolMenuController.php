<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/11/2019 10:13 AM
 * @Updated: 3/11/2019 10:13 AM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School\App\Http\Controllers;

use Illuminate\Http\Request;
use Natenju\School\Facades\School;

/**
 * Class SchoolMenuController
 *
 * @package Natenju\School\app\Http\Controllers
 */
class SchoolMenuController extends Controller {
    public function builder($id) {
        $menu = School::model('Menu')->findOrFail($id);
        
        $this->authorize('edit', $menu);
        
        $isModelTranslatable = is_bread_translatable(School::model('MenuItem'));
        
        return School::view('school::menus.builder', compact('menu', 'isModelTranslatable'));
    }
    
    public function delete_menu($menu, $id) {
        $item = School::model('MenuItem')->findOrFail($id);
        
        $this->authorize('delete', $item);
        
        $item->deleteAttributeTranslation('title');
        
        $item->destroy($id);
        
        return redirect()
            ->route('school.menus.builder', [$menu])
            ->with(
                [
                    'message'    => __('school::menu_builder.successfully_deleted'),
                    'alert-type' => 'success',
                ]
            );
    }
    
    public function add_item(Request $request) {
        $menu = School::model('Menu');
        
        $this->authorize('add', $menu);
        
        $data = $this->prepareParameters(
            $request->all()
        );
        
        unset($data['id']);
        $data['order'] = School::model('MenuItem')->highestOrderMenuItem();
        
        // Check if is translatable
        $_isTranslatable = is_bread_translatable(School::model('MenuItem'));
        if ( $_isTranslatable ) {
            // Prepare data before saving the menu
            $trans = $this->prepareMenuTranslations($data);
        }
        
        $menuItem = School::model('MenuItem')->create($data);
        
        // Save menu translations
        if ( $_isTranslatable ) {
            $menuItem->setAttributeTranslations('title', $trans, TRUE);
        }
        
        return redirect()
            ->route('school.menus.builder', [$data['menu_id']])
            ->with(
                [
                    'message'    => __('school::menu_builder.successfully_created'),
                    'alert-type' => 'success',
                ]
            );
    }
    
    public function update_item(Request $request) {
        $id = $request->input('id');
        $data = $this->prepareParameters(
            $request->except(['id'])
        );
        
        $menuItem = School::model('MenuItem')->findOrFail($id);
        
        $this->authorize('edit', $menuItem->menu);
        
        if ( is_bread_translatable($menuItem) ) {
            $trans = $this->prepareMenuTranslations($data);
            
            // Save menu translations
            $menuItem->setAttributeTranslations('title', $trans, TRUE);
        }
        
        $menuItem->update($data);
        
        return redirect()
            ->route('school.menus.builder', [$menuItem->menu_id])
            ->with(
                [
                    'message'    => __('school::menu_builder.successfully_updated'),
                    'alert-type' => 'success',
                ]
            );
    }
    
    public function order_item(Request $request) {
        $menuItemOrder = json_decode($request->input('order'));
        
        $this->orderMenu($menuItemOrder, NULL);
    }
    
    private function orderMenu(array $menuItems, $parentId) {
        foreach ( $menuItems as $index => $menuItem ) {
            $item = School::model('MenuItem')->findOrFail($menuItem->id);
            $item->order = $index + 1;
            $item->parent_id = $parentId;
            $item->save();
            
            if ( isset($menuItem->children) ) {
                $this->orderMenu($menuItem->children, $item->id);
            }
        }
    }
    
    protected function prepareParameters($parameters) {
        switch ( array_get($parameters, 'type') ) {
            case 'route':
                $parameters['url'] = NULL;
                break;
            default:
                $parameters['route'] = NULL;
                $parameters['parameters'] = '';
                break;
        }
        
        if ( isset($parameters['type']) ) {
            unset($parameters['type']);
    }
        
        return $parameters;
    }
    
    /**
     * Prepare menu translations.
     *
     * @param array $data menu data
     *
     * @return mixed translated item
     */
    protected function prepareMenuTranslations(&$data) {
        $trans = json_decode($data['title_i18n'], TRUE);
        
        // Set field value with the default locale
        $data['title'] = $trans[config('school.multilingual.default', 'en')];
        
        unset($data['title_i18n']);     // Remove hidden input holding translations
        unset($data['i18n_selector']);  // Remove language selector input radio
        
        return $trans;
    }
}