<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/11/2019 10:25 AM
 * @Updated: 3/11/2019 10:25 AM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School\App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use League\Flysystem\Plugin\ListWith;
use Natenju\School\Facades\School;

/**
 * Class SchoolMediaController
 *
 * @package Natenju\School\app\Http\Controllers
 */
class SchoolMediaController extends Controller {
    
    /** @var string */
    private $filesystem;
    
    /** @var string */
    private $directory = '';
    
    public function __construct() {
        $this->filesystem = config('school.storage.disk');
    }
    
    public function index() {
        // Check permission
        School::canOrFail('browse_media');
        
        return view('school::media.index');
    }
    
    public function files(Request $request) {
        // Check permission
        School::canOrFail('browse_media');
        
        $folder = $request->folder;
        
        if ( $folder == '/' ) {
            $folder = '';
        }
        
        $dir = $this->directory . $folder;
        
        return response()->json(
            [
                'name'          => 'files',
                'type'          => 'folder',
                'path'          => $dir,
                'folder'        => $folder,
                'items'         => $this->getFiles($dir),
                'last_modified' => 'asdf',
            ]
        );
    }
    
    // New Folder with 5.3

    private function getFiles($dir) {
        // Check permission
        School::canOrFail('browse_media');
        
        $files = [];
        $storage = Storage::disk($this->filesystem)->addPlugin(new ListWith());
        $storageItems = $storage->listWith(['mimetype'], $dir);
        
        foreach ( $storageItems as $item ) {
            if ( $item['type'] == 'dir' ) {
                $files[] = [
                    'name'          => $item['basename'],
                    'type'          => 'folder',
                    'path'          => Storage::disk($this->filesystem)->url($item['path']),
                    'items'         => '',
                    'last_modified' => '',
                ];
            } else {
                if ( empty(pathinfo($item['path'], PATHINFO_FILENAME)) && !config('school.hidden_files') ) {
                    continue;
                }
                $files[] = [
                    'name'          => $item['basename'],
                    'type'          => isset($item['mimetype']) ? $item['mimetype'] : 'file',
                    'path'          => Storage::disk($this->filesystem)->url($item['path']),
                    'size'          => $item['size'],
                    'last_modified' => $item['timestamp'],
                ];
            }
        }
        
        return $files;
    }
    
    // Delete File or Folder with 5.3

    public function new_folder(Request $request) {
        // Check permission
        School::canOrFail('browse_media');
        
        $new_folder = $request->new_folder;
        $success = FALSE;
        $error = '';
        
        if ( Storage::disk($this->filesystem)->exists($new_folder) ) {
            $error = __('school::media.folder_exists_already');
        } elseif ( Storage::disk($this->filesystem)->makeDirectory($new_folder) ) {
            $success = TRUE;
        } else {
            $error = __('school::media.error_creating_dir');
        }
        
        return compact('success', 'error');
    }
    
    // GET ALL DIRECTORIES Working with Laravel 5.3

    public function delete_file_folder(Request $request) {
        // Check permission
        School::canOrFail('browse_media');
        
        $folderLocation = $request->folder_location;
        $fileFolder = $request->file_folder;
        $type = $request->type;
        $success = TRUE;
        $error = '';
        
        if ( is_array($folderLocation) ) {
            $folderLocation = rtrim(implode('/', $folderLocation), '/');
        }
        
        $location = "{$this->directory}/{$folderLocation}";
        $fileFolder = "{$location}/{$fileFolder}";
        
        if ( $type == 'folder' ) {
            if ( !Storage::disk($this->filesystem)->deleteDirectory($fileFolder) ) {
                $error = __('school::media.error_deleting_folder');
                $success = FALSE;
            }
        } elseif ( !Storage::disk($this->filesystem)->delete($fileFolder) ) {
            $error = __('school::media.error_deleting_file');
            $success = FALSE;
        }
        
        return compact('success', 'error');
    }
    
    // NEEDS TESTING

    public function get_all_dirs(Request $request) {
        // Check permission
        School::canOrFail('browse_media');
        
        $folderLocation = $request->folder_location;
        
        if ( is_array($folderLocation) ) {
            $folderLocation = rtrim(implode('/', $folderLocation), '/');
        }
        
        $location = "{$this->directory}/{$folderLocation}";
        
        return response()->json(
            str_replace($location, '', Storage::disk($this->filesystem)->directories($location))
        );
    }
    
    // RENAME FILE WORKING with 5.3

    public function move_file(Request $request) {
        // Check permission
        School::canOrFail('browse_media');
        
        $source = $request->source;
        $destination = $request->destination;
        $folderLocation = $request->folder_location;
        $success = FALSE;
        $error = '';
        
        if ( is_array($folderLocation) ) {
            $folderLocation = rtrim(implode('/', $folderLocation), '/');
        }
        
        $location = "{$this->directory}/{$folderLocation}";
        $source = "{$location}/{$source}";
        $destination = strpos($destination, '/../') !== FALSE
            ? $this->directory . '/' . dirname($folderLocation) . '/' . str_replace('/../', '', $destination)
            : "/{$destination}";
        
        if ( !file_exists($destination) ) {
            if ( Storage::disk($this->filesystem)->move($source, $destination) ) {
                $success = TRUE;
            } else {
                $error = __('school::media.error_moving');
            }
        } else {
            $error = __('school::media.error_already_exists');
        }
        
        return compact('success', 'error');
    }
    
    // Upload Working with 5.3

    public function rename_file(Request $request) {
        // Check permission
        School::canOrFail('browse_media');
        
        $folderLocation = $request->folder_location;
        $filename = $request->filename;
        $newFilename = $request->new_filename;
        $success = FALSE;
        $error = FALSE;
        
        if ( is_array($folderLocation) ) {
            $folderLocation = rtrim(implode('/', $folderLocation), '/');
        }
        
        $location = "{$this->directory}/{$folderLocation}";
        
        if ( !Storage::disk($this->filesystem)->exists("{$location}/{$newFilename}") ) {
            if ( Storage::disk($this->filesystem)->move("{$location}/{$filename}", "{$location}/{$newFilename}") ) {
                $success = TRUE;
            } else {
                $error = __('school::media.error_moving');
            }
        } else {
            $error = __('school::media.error_may_exist');
        }
        
        return compact('success', 'error');
    }
    
    public function upload(Request $request) {
        // Check permission
        School::canOrFail('browse_media');
        
        try {
            $realPath = Storage::disk($this->filesystem)->getDriver()->getAdapter()->getPathPrefix();
            
            $allowedImageMimeTypes = [
                'image/jpeg',
                'image/png',
                'image/gif',
                'image/bmp',
                'image/svg+xml',
            ];
            $file = $request->file->store($request->upload_path, $this->filesystem);
            
            if ( in_array($request->file->getMimeType(), $allowedImageMimeTypes) ) {
                $image = Image::make($realPath . $file);
                
                if ( $request->file->getClientOriginalExtension() == 'gif' ) {
                    copy($request->file->getRealPath(), $realPath . $file);
                } else {
                    $image->orientate()->save($realPath . $file);
                }
            }
            
            $success = TRUE;
            $message = __('school::media.success_uploaded_file');
            $path = preg_replace('/^public\//', '', $file);
        } catch ( Exception $e ) {
            $success = FALSE;
            $message = $e->getMessage();
            $path = '';
        }
        
        return response()->json(compact('success', 'message', 'path'));
    }
    
    // REMOVE FILE

    public function remove(Request $request) {
        // Check permission
        School::canOrFail('browse_media');
        // Check permission
        School::canOrFail('delete_media');
        //TODO: Delete file or folder recursively and properly
    }
    
    // Crop Image
    public function crop(Request $request) {
        // Check permission
        School::canOrFail('browse_media');
        
        $createMode = $request->get('createMode') === 'true';
        $x = $request->get('x');
        $y = $request->get('y');
        $height = $request->get('height');
        $width = $request->get('width');
        
        $realPath = Storage::disk($this->filesystem)->getDriver()->getAdapter()->getPathPrefix();
        $originImagePath = $realPath . $request->upload_path . '/' . $request->originImageName;
        
        try {
            if ( $createMode ) {
                // create a new image with the cpopped data
                $fileNameParts = explode('.', $request->originImageName);
                array_splice($fileNameParts, count($fileNameParts) - 1, 0, 'cropped_' . time());
                $newImageName = implode('.', $fileNameParts);
                $destImagePath = $realPath . $request->upload_path . '/' . $newImageName;
            } else {
                // override the original image
                $destImagePath = $originImagePath;
            }
            
            Image::make($originImagePath)->crop($width, $height, $x, $y)->save($destImagePath);
            
            $success = TRUE;
            $message = __('school::media.success_crop_image');
        } catch ( Exception $e ) {
            $success = FALSE;
            $message = $e->getMessage();
        }
        
        return response()->json(compact('success', 'message'));
    }
}