<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/11/2019 9:55 AM
 * @Updated: 3/11/2019 9:55 AM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School\App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Class SchoolRoleController
 *
 * @package Natenju\School\app\Http\Controllers
 */
class SchoolRoleController extends SchoolBaseController {
    public function update(Request $request, $id) {
        //TODO: write SchoolRoleController@update content
    }
    
    public function store(Request $request){
        //TODO: write SchoolRoleController@store content
    }
}