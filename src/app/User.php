<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 11:05 PM
 * @Updated: 3/10/2019 11:05 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School\app;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Natenju\School\Contracts\User as UserContract;

class User extends Authenticatable implements UserContract{
    use VoyagerUser;
    
    protected $guarded = [];
    
    protected $casts = [
        'settings' => 'array',
    ];
    
    public function getAvatarAttribute($value) {
        if ( is_null($value) ) {
            return config('school.user.default_avatar', 'users/default.png');
        }
        
        return $value;
    }
    
    public function setCreatedAtAttribute($value) {
        $this->attributes['created_at'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }
    
    public function setLocaleAttribute($value) {
        $this->attributes['settings'] = collect($this->settings)->merge(['locale' => $value]);
    }
    
    public function getLocaleAttribute() {
        return $this->settings['locale'];
    }
}