<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 10:58 PM
 * @Updated: 3/10/2019 10:58 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School\app;


use Illuminate\Database\Eloquent\Model;
use Natenju\School\Facades\School as schoolFacade;

class Role extends Model {
    protected $guarded = [];
    
    public function users() {
        $userModel = schoolFacade::modelClass('User');
        
        return $this->belongsToMany($userModel, 'user_roles')
                    ->select(app($userModel)->getTable() . '.*')
                    ->union($this->hasMany($userModel))->getQuery();
    }
    
    public function permissions() {
        return $this->belongsToMany(schoolFacade::modelClass('Permission'));
    }
}