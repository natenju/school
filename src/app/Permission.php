<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 10:55 PM
 * @Updated: 3/10/2019 10:55 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School\app;


use Illuminate\Database\Eloquent\Model;
use Natenju\School\Facades\School as schoolFacade;

class Permission extends Model {
    protected $guarded = [];
    
    public function roles() {
        return $this->belongsToMany(SchoolFacade::modelClass('Role'));
    }
    
    public static function generateFor($table_name) {
        self::firstOrCreate(['key' => 'browse_' . $table_name, 'table_name' => $table_name]);
        self::firstOrCreate(['key' => 'read_' . $table_name, 'table_name' => $table_name]);
        self::firstOrCreate(['key' => 'edit_' . $table_name, 'table_name' => $table_name]);
        self::firstOrCreate(['key' => 'add_' . $table_name, 'table_name' => $table_name]);
        self::firstOrCreate(['key' => 'delete_' . $table_name, 'table_name' => $table_name]);
    }
    
    public static function removeFrom($table_name) {
        self::where(['table_name' => $table_name])->delete();
    }
}