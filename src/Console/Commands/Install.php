<?php

namespace Natenju\School\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Route;
use Natenju\School\School;
use Natenju\School\SchoolDemoServiceProvider;
use Natenju\School\SchoolServiceProvider;
use Natenju\School\Traits\Seedable;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Process\Process;

class Install extends Command {
    use Seedable;
    
    protected $seedersPath = __DIR__ . '/../../../demo/database/seeds/';
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'school:install {--d|demo : install with demonstration data}';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install school using CLI';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @param \Illuminate\Filesystem\Filesystem $filesystem
     *
     * @return void
     */
    public function handle(Filesystem $filesystem) {
        
        $this->header();
        
        $this->checkRequirements();
        
        $this->info("Launching your app installation");
        if ( $this->confirm('Do you have setting the database configuration at .env ?', TRUE) ) {
            
            $this->info('Dumping the autoloaded files and reloading all new files...');
            $composer = $this->findComposer();
            $process = new Process([$composer . ' dumpautoload'], base_path(), NULL, NULL, NULL);
            $process->run();
            
            // Publish only relevant resources on install
            $tags = ['demo_assets', 'seeds'];
            $this->info('Publishing the assets, database, and config files');
            $this->call('vendor:publish', ['--provider' => SchoolServiceProvider::class, '--tag' => $tags]);
            
            $this->info('Migrating database...');
            $this->call('migrate');
            
            $this->info('Adding routes to routes/web.php');
            try {
                $routes_contents = $filesystem->get(base_path('routes/web.php'));
            } catch ( FileNotFoundException $e ) {
                $this->error(base_path('routes/web.php')." not found\n".$e->getMessage());
            }
            if ( FALSE === strpos($routes_contents, 'School::routes()') ) {
                $filesystem->append(
                    base_path('routes/web.php'),
                    "\n\nRoute::group(['prefix' => 'admin'], function () {\n    School::routes();\n});\n"
                );
            }
            
            Route::group(
                ['prefix' => 'school'],
                function () {
                    School::routes();
                }
            );
            
            $this->info('Seeding data into the database');
            $this->seed('SchoolDatabaseSeeder');
            if ( $this->option('demo') ) {
                $this->info('Publishing demo content');
                $tags = ['demo_seeds', 'demo_content', 'demo_config', 'demo_migrations'];
                $this->call('vendor:publish', ['--provider' => SchoolDemoServiceProvider::class, '--tag' => $tags]);
                
                $this->info('Migrating demo tables');
                $this->call('migrate');
                
                $this->info('Seeding demo data');
                $this->seed('SchoolDemoDatabaseSeeder');
            } else {
                $this->call(
                    'vendor:publish',
                    ['--provider' => SchoolServiceProvider::class, '--tag' => ['config']]
                );
            }
            
            $this->call('config:clear');
            $this->call('optimize');
            
            $this->footer();
            $this->info('Thank You :)');
        } else {
            $this->footer(FALSE);
            $this->info('Setup Aborted !');
            $this->info('Please setting the database configuration for first !');
        }
    }
    
    /**
     * Get the composer command for the environment.
     *
     * @return string
     */
    protected function findComposer() {
        if ( file_exists(getcwd() . '/composer.phar') ) {
            return '"' . PHP_BINARY . '" ' . getcwd() . '/composer.phar';
        }
        
        return 'composer';
    }
    
    protected function getOptions() {
        return [
            ['demo', NULL, InputOption::VALUE_NONE, 'Install with demo data', NULL],
        ];
    }
    
    /**
     * @param \Illuminate\Filesystem\Filesystem $filesystem
     */
    public function fire(Filesystem $filesystem) {
        $this->handle($filesystem);
    }
    
    private function header() {
        $this->info("--------- setup installing " . env('APP_NAME', "App") . " ---------------");
        
    }
    
    private function checkRequirements() {
        $this->info('System Requirements Checking:');
        $system_failed = 0;
        $laravel = app();
        
        if ( $laravel::VERSION >= 5.8 ) {
            $this->info('Laravel Version (>= 5.8.*): [Good]');
        } else {
            $this->info('Laravel Version (< 5.8.*): [Bad]');
            $system_failed++;
        }
        
        if ( is_writable(base_path('public')) ) {
            $this->info('public dir is writable: [Good]');
        } else {
            $this->info('public dir is writable: [Bad]');
            $system_failed++;
        }
        
        if ( $system_failed != 0 ) {
            $this->info('Sorry unfortunately your system did not meet with our requirements !');
            $this->footer(FALSE);
        }
    }
    
    /**
     * footer
     *
     * @param  mixed $success
     *
     * @return void
     */
    private function footer($success = TRUE) {
        $this->info('====================================================================');
        if ( $success == TRUE ) {
            $this->info('-------------------  Completed !!  ------------------------');
        } else {
            $this->info('-------------------    Failed !!   ------------------------');
        }
        exit;
    }
    
}
