<?php

namespace Natenju\School\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class Update extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'school:update';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Your app';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle() {
        $this->header();
        
        $this->checkRequirements();
        
        $this->info("Launching your app update");
        $this->warn("We recommend that you backup you database before continueing");
        if ( $this->confirm("Shall we continue?") ) {
            $this->info('Dumping the auto loaded files and reloading all new files...');
            $composer = $this->findComposer();
            $process = new Process([$composer . ' dump-autoload'], base_path(), NULL, NULL, NULL);
            $process->run();
            
            //TODO:Updating this package
            
        } else {
            $this->footer(FALSE);
            $this->info('Setup Aborted !');
        }
    }
    
    private function header() {
        $this->info("--------- setup installing" . env('APP_NAME', "App") . " ---------------");
        
    }
    
    private function checkRequirements() {
        $this->info('System Requirements Checking:');
        $system_failed = 0;
        
        if ( $system_failed != 0 ) {
            $this->info('Sorry unfortunately your system did not meet with our requirements !');
            $this->footer(FALSE);
        }
    }
    
    /**
     * footer
     *
     * @param  mixed $success
     *
     * @return void
     */
    private function footer($success = TRUE) {
        $this->info('====================================================================');
        if ( $success == TRUE ) {
            $this->info('-------------------  Completed !!  ------------------------');
        } else {
            $this->info('-------------------    Failed !!   ------------------------');
        }
        exit;
    }
    
    /**
     * Get the composer command for the environment.
     *
     * @return string
     */
    protected function findComposer() {
        if ( file_exists(getcwd() . '/composer.phar') ) {
            return '"' . PHP_BINARY . '" ' . getcwd() . '/composer.phar';
        }
        
        return 'composer';
    }
}
