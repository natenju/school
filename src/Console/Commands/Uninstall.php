<?php

namespace Natenju\School\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class Uninstall extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'school:uninstall';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Uninstall Your app';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle() {
    
        $this->header();
    
        $this->checkRequirements();
    
        $this->warn("this will completely delete your app. continue");
    
        if ( $this->confirm("continue?") ) {
            $this->info("Launching your app uninstall");
        
            $composer = $this->findComposer();
            $process = new Process([$composer . ' dumpautoload'], base_path(), NULL, NULL, NULL);
            $process->run();
        
            $this->info('Rolling back database...');
            // TODO:unload migration files here
            $this->call('migrate:rollback');
        
            $this->call('config:clear');
            $this->call('optimize');
        
            $this->info('Installing Your Admin Is Completed ! Thank You :)');
        }
    
        $this->footer();
    }
    
    /**
     * Get the composer command for the environment.
     *
     * @return string
     */
    protected function findComposer() {
        if ( file_exists(getcwd() . '/composer.phar') ) {
            return '"' . PHP_BINARY . '" ' . getcwd() . '/composer.phar';
        }
        
        return 'composer';
    }
    
    private function header() {
        $this->info("--------- uninstalling" . env('APP_NAME', "App") . " ---------------");
    }
    
    private function checkRequirements() {
    
    }
    
    private function footer($success = TRUE) {
        $this->info('====================================================================');
        if ( $success == TRUE ) {
            $this->info('-------------------  Completed !!  ------------------------');
        } else {
            $this->info('-------------------    Failed !!   ------------------------');
        }
        exit;
    }
}
