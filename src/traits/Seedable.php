<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 4:20 PM
 * @Updated: 3/10/2019 4:20 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\School\Traits;


trait Seedable {
    public function seed($class) {
        if ( !class_exists($class) ) {
            require_once $this->seedersPath . $class . '.php';
        }
        
        with(new $class())->run();
    }
}